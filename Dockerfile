FROM php:7.4-fpm-buster

MAINTAINER Ryan Rempel <rgrempel@cmu.ca>

RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"

ENV DEBIAN_FRONTEND=noninteractive \
    MOODLE_VERSION=311 \
    ACCEPT_EULA=Y \
    BASE_DATE=20220724

WORKDIR /var/www/html

RUN apt-get -qq update && apt-get -qq -y --no-install-recommends install \
    git libpq-dev zlib1g-dev libpng-dev libjpeg-dev libfreetype6-dev libxml2-dev libzip-dev libldap2-dev libxslt1-dev gnupg2

RUN docker-php-ext-configure gd --with-freetype --with-jpeg && \
    docker-php-ext-install -j$(nproc) \
      pdo_pgsql pgsql exif gd xsl xmlrpc soap zip intl opcache ldap

RUN curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add - && \
    curl https://packages.microsoft.com/config/debian/10/prod.list > /etc/apt/sources.list.d/mssql-release.list

RUN apt-get -qq update && apt-get -qq -y --no-install-recommends install \
    msodbcsql17 unixodbc-dev

RUN pecl install sqlsrv pdo_sqlsrv && \
    docker-php-ext-enable sqlsrv pdo_sqlsrv

RUN apt-get -qq update && apt-get -qq -y --no-install-recommends install \
    sassc unzip locales unoconv clamdscan aspell aspell-en graphviz ghostscript python3 sudo procps gettext-base

COPY locale.gen /etc/locale.gen

RUN dpkg-reconfigure --frontend=noninteractive locales \
    && update-locale LANG=en_US.UTF-8

ENV LANG en_CA.UTF-8

RUN git clone --depth 1 -b MOODLE_${MOODLE_VERSION}_STABLE git://git.moodle.org/moodle.git /var/www/html

RUN git clone --depth 1 -b MOODLE_36_STABLE https://github.com/itamart/moodle-block_mhaairs.git blocks/mhaairs

RUN curl -s -L -o plugin.zip https://s3.amazonaws.com/ThirdPartySolutions/standard/moodle/pearsonstandard-2019012000.zip \
    && unzip plugin.zip \
    && rm plugin.zip \
    && tar -C pearsonstandard-* -cf - . | tar -xf - \
    && rm -r pearsonstandard-*

COPY install-plugin.sh /install-plugin.sh

RUN /install-plugin.sh local       26886/local_o365_moodle311_2021051725.zip
RUN /install-plugin.sh auth        26880/auth_oidc_moodle311_2021051725.zip
RUN /install-plugin.sh repository  26352/repository_office365_moodle311_2021051720.zip
RUN /install-plugin.sh blocks      26343/block_microsoft_moodle311_2021051720.zip
RUN /install-plugin.sh theme       26355/theme_boost_o365teams_moodle311_2021051720.zip

RUN /install-plugin.sh mod         25667/mod_oublog_moodle311_2020091403.zip
RUN /install-plugin.sh theme       27057/theme_boost_campus_moodle311_2021122802.zip
RUN /install-plugin.sh lib/editor  24425/editor_marklar_moodle311_2021060900.zip && mv lib/editor/mudrd8mz-moodle-editor_marklar-98ed92e lib/editor/marklar
RUN /install-plugin.sh lib/editor/atto/plugins 25051/atto_wordimport_moodle311_2021083100.zip
RUN /install-plugin.sh lib/editor/atto/plugins 17250/atto_count_moodle35_2018062700.zip
RUN /install-plugin.sh mod/book/tool 25049/booktool_wordimport_moodle311_2021083100.zip
RUN /install-plugin.sh mod         20738/mod_scheduler_moodle39_2019120200.zip
RUN /install-plugin.sh mod         24871/mod_questionnaire_moodle311_2020111101.zip

COPY templates/core/loginform.mustache theme/boost_campus/templates/core/loginform.mustache
COPY pix/favicon.ico theme/boost_campus/pix/favicon.ico
