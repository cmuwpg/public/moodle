#!/bin/bash

cd /var/www/html/$1 \
    && curl -s -L -o plugin.zip https://moodle.org/plugins/download.php/$2 \
    && unzip plugin.zip \
    && rm plugin.zip
